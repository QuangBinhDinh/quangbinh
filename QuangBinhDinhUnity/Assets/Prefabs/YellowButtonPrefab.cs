﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class YellowButtonPrefab : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_title;

    [SerializeField] TextMeshProUGUI m_index;

   
    public void changeText(string index, string title)
    {
        m_index.text = index;
        m_title.text = title;

    }
}
