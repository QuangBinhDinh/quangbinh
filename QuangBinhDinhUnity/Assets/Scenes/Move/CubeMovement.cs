﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    // Start is called before the first frame update

  
    public float speed;

   
    Vector3 direct;

    Ray ray;

    RaycastHit hit;

    string m_tagNoRigid = "NoRigid";
    string m_tagRigid = "HaveRigid";


    Rigidbody rb;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        
        if (Input.GetMouseButton(0))
        {

             ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {

                if (hit.transform.CompareTag(m_tagNoRigid) || hit.transform.CompareTag(m_tagRigid))
                {
                    direct = hit.transform.position - transform.position;
                    rb.velocity = direct.normalized * speed;
                    //rb.MovePosition(transform.position + directionNoRigid.normalized * speed * Time.deltaTime);
                    //transform.Translate(direct.normalized * speed * Time.deltaTime , Space.World);
                   

                    //điểm khác biệt: move obj = velocity sẽ có gia tốc và có quán tính, trong khi move = MovePosition thì không
                } 
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            
            rb.velocity = Vector3.zero;
        }

    }
    //void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log(collision.collider.name);
    //    if (collision.collider.name == "CubeNoRigid" || collision.collider.name == "CubeWithRigid") moving = false;
    //}
}
