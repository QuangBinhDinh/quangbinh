﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMove : MonoBehaviour
{
    Rigidbody rb;

    Renderer render;

    [SerializeField] TextMeshProUGUI m_text;

    [SerializeField] float m_playerSpeed;

    [SerializeField] Animator anim;

    [HideInInspector]
    public bool m_GamePause;

    [HideInInspector]
    public bool m_GameOver;

    [HideInInspector]
    public bool m_GameWin;

    enum PlayerState
    {
        Idle,Active
    }

    PlayerState m_state;



    bool m_canMove;

    bool m_freeMove;

    bool m_playerSelected;


    bool m_timeRunning;

    float m_timeRemain;//tinh theo giay

    float m_timeIdle;

    float m_timeActive;

    string m_tagPlayer = "Player";

    string m_tagPlane = "Plane";

    string m_tagFinish = "Finish";


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
        render.material.SetColor("_Color", Color.white);

        m_GamePause = false;
        m_GameOver = false;
        m_GameWin = false;

       
        m_freeMove = true;//freeMove = true thi chua the di chuyen theo chuot
        m_playerSelected = false;

        m_timeRemain = 100f; 
        m_timeRunning = true;

        m_state = PlayerState.Idle;

        StartCoroutine(checkGameStatus());

        StartCoroutine(HandleState());
        
    }

  
    void Update()
    {

        if (Input.GetMouseButton(0) && m_canMove)//ghi nho: getMouseButton phai dung trong Update, dung trong FixUpdate thi thoang se bi miss
        {
           
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

           
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (hit.transform.CompareTag(m_tagPlayer))
                {//case user nhan chuot vao player, player di chuyen theo con chuot
                    m_playerSelected = true;
                    rb.velocity = Vector3.zero;
                    m_freeMove = false;
                    transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);


                }
                else if (hit.transform.CompareTag(m_tagPlane) && !m_freeMove && m_playerSelected)
                {
                    //truong hop chuot di chuyen qua nhanh
                    transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                }
                else m_playerSelected = false;
            }

            if (m_freeMove)
            {
                rb.velocity = transform.forward * m_playerSpeed;
                Debug.Log(rb.velocity);

            }
           
        }
        else if (Input.GetMouseButtonUp(0) && m_canMove)
        {
            if (m_freeMove)
            {
                rb.velocity = Vector3.zero;
                
            }
            else m_playerSelected = false;
        }

    }

    void OnCollisionEnter(Collision collision)//xu ly va cham
    {
        if(collision.transform.CompareTag(m_tagFinish))
        {
            m_timeRunning = false;
            m_GameWin = true;
 
        }
    }

    void DisplayTimeLeft(float timeLeft)
    {
        //hien thi countdown
        timeLeft += 1;
        float minutes = Mathf.FloorToInt(timeLeft / 60);
        float seconds = Mathf.FloorToInt(timeLeft % 60);
        m_text.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

   

    IEnumerator HandleState()
    {
        while (!m_GameOver || !m_GameWin)
        {
            switch (m_state)
            {
                case PlayerState.Idle:
                    m_canMove = false;
                    rb.velocity = Vector3.zero;
                    render.material.SetColor("_Color", Color.white);
                    anim.enabled = true;
                    yield return new WaitForSeconds(1.5f);
                    m_state = PlayerState.Active;
                    break;
                case PlayerState.Active:
                    m_canMove = true;
                    render.material.SetColor("_Color", Color.green);
                    anim.enabled = false;
                    yield return new WaitForSeconds(1f);
                    m_state = PlayerState.Idle;
                    break;
            }
        }
    }

    IEnumerator checkGameStatus()
    {
        while (true)
        {
            if (m_timeRunning && m_timeRemain > 0f)
            {
                m_timeRemain -= Time.deltaTime;
                DisplayTimeLeft(m_timeRemain);
            }
            else if (m_timeRemain <= 0f)
            {
                m_text.text = "Time out!";
            }

            if (m_GamePause)
            {

                m_text.text = "Game paused";
                Time.timeScale = 0;

            }
            else
            {
                Time.timeScale = 1;

            }

            if (m_GameOver)
            {
                
                m_text.text = "Game Over!";
                anim.enabled = false;
                m_canMove = false;
                rb.velocity = Vector3.zero;
                Debug.Log(m_text.text);
                StopAllCoroutines();
            }

            if (m_GameWin)
            {
                m_canMove = false;
                m_text.text = "You win !";
                anim.enabled = false;
                rb.velocity = Vector3.zero;
                StopAllCoroutines();

            }

            yield return null;

        }

    }

   
}
