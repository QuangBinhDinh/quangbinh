﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate1Script : MonoBehaviour
{
    // Start is called before the first frame update
    
    [SerializeField] Transform cubeTrans;
    [SerializeField] Camera cam;

    Ray ray;
    RaycastHit hit;

    bool m_isRotating = false;

    string m_tagPlane = "Plane";

   

    Vector3 m_initialVector;

    Vector3 m_desVector;


    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {

            ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {

                if (hit.transform.CompareTag(m_tagPlane))
                {
                    
                    m_initialVector = CalculateVector(hit.point);
                }
            }
                
        }
        else if (Input.GetMouseButton(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {

                if (hit.transform.CompareTag(m_tagPlane))
                {
                    m_desVector = CalculateVector(hit.point);

                    CubeRotate(m_initialVector, m_desVector);

                    m_initialVector = m_desVector;
                }
            }
        }
    }

    Vector3 CalculateVector(Vector3 hitpoint)
    {
        Debug.DrawRay(cubeTrans.position, hitpoint - cubeTrans.position, Color.black);
        return hitpoint - cubeTrans.position;
    }

    void CubeRotate(Vector3 from, Vector3 to)

    {
        Debug.Log(from + " " + to);
        float rotateAngle = Vector3.SignedAngle(from, to, transform.forward);
        Debug.Log(rotateAngle);
        cubeTrans.Rotate(0f, 0f, rotateAngle);
    }
}
