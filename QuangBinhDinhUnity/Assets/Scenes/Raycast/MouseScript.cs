﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour
{
    // Start is called before the first frame update

    

    RectTransform rect;
    void Awake()
    {
        Cursor.visible = false;
        rect = GetComponent<RectTransform>();
    }

    
    void Update()
    {

        rect.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

        if (Input.GetMouseButtonDown(0)) Cursor.visible = false;
    }
}
