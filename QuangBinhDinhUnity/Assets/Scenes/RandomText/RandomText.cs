﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomText : MonoBehaviour
{
    [SerializeField] YellowButtonPrefab m_prefab;

    [SerializeField] string[] randomTextList;

    [SerializeField] int number;

    void Awake()
    {
        for(int i =1;i <= number ; i++)
        {
            YellowButtonPrefab temp_prefab = Instantiate(m_prefab, transform);

            string title = randomTextList[Random.Range(0, randomTextList.Length)];
            
            
            temp_prefab.changeText(i.ToString(), title);
        }
        
    }

   
}
