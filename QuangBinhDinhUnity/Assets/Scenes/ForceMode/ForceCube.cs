﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceCube : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward, ForceMode.VelocityChange);
        
        //nên nhớ : chỉ gọi VelocityChange hay Impluse ở hàm start do bản chất của hàm là cộng thêm vận tốc thẳng vào velocity
        //tương ứng với rb.velocity += transform.forward
    }

    
    void FixedUpdate()
    {
        rb.AddForce(transform.forward, ForceMode.Acceleration);
        //chỉ gọi Accel hay Force ở hàm update do hàm này sẽ cộng thêm vận tốc sau 1s
        //tương ứng với rb.velocity += transform.forward * Time.DeltaTime
    }
}
