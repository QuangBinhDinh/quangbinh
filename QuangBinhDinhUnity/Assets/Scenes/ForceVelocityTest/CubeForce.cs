﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeForce : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
   

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(transform.up, speed * Time.deltaTime,Space.World);
    }
}
