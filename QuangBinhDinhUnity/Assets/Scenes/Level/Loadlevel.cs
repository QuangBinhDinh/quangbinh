﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loadlevel : MonoBehaviour
{
    // Script nay gan vao content cua scroll view


    [Tooltip("so luong can render")]
    public int number;
    
    [Tooltip("prefab can duoc chon")]
    [SerializeField] LvPrefab prefab;

    
    
    void Start()
    {
       
        for (int i = 1; i <= number; i++)
        {
            LvPrefab temp = Instantiate(prefab, transform);

            temp.ModifyLevel(i, Random.Range(0, 4));
            
        }
    }

 
}
