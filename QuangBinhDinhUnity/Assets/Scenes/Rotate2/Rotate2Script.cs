﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate2Script : MonoBehaviour
{//script này được gắn vào mainCamera
    private Camera cam;

    private Vector3 mouseEndPos;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        ;
    }
   
    
    void Update()
    {
       
        if (Input.GetMouseButtonDown(0))
        {

            mouseEndPos = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {

            float rotateDegree;
            Vector3 cameraRotate = transform.rotation.eulerAngles;
            float percentageWidth = (Input.mousePosition.x - mouseEndPos.x) / cam.pixelWidth;

            rotateDegree = percentageWidth * 90;//nếu percentage là 1, tức là chuột di chuyển toàn chiều rộng màn hình thì camera sẽ quay 1 góc 90 dộ
            //có thể coi là speed quay camera
            mouseEndPos = Input.mousePosition;

            //cứ mỗi frame lấy mousePos ở 2 vị trí, tính percentage theo screen width và chuyển thành góc quay
            //vị trí kết thúc chuột của frame này sẽ thành vị trí bắt đầu ở frame sau

            float wrapAngleY = WrapAngle(cameraRotate.y);

            if ((rotateDegree < 0 && wrapAngleY <= 45 ) || (rotateDegree > 0 && wrapAngleY >= -45))
            {
                //quay camera với limit mỗi góc bên trái và bên phải 45 độ
                cameraRotate.y -= rotateDegree;
                transform.rotation = Quaternion.Euler(cameraRotate);
               
            }
        }
        
    }


    private static float WrapAngle(float angle)
    {//do vector Quaternion có range (0,360) khác với trong inspector (-180,180) nên dùng hàm này để chuyển từ Quaternion -> inspector
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }

    private static float UnwrapAngle(float angle)
    {//chuyển ngược lại
        if (angle >= 0)
            return angle;

        angle = -angle % 360;

        return 360 - angle;
    }
}

