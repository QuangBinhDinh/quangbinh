﻿
using UnityEngine;

using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

public class MoveAround : MonoBehaviour
{

    public PathType type = PathType.CatmullRom;

    public Transform sphere;

    public Vector3[] waypoint;

    Tween t;


  
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CamMove());
    }


    IEnumerator CamMove()
    {
        yield return new WaitForSeconds(1.5f);
        t = transform.DOPath(waypoint, 4f, type).SetOptions(false);
        t.SetEase(Ease.OutQuad);

    }


}
